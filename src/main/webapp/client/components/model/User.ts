export default class User {
    private _name:string;
    private _role:string;

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get role(): string {
        return this._role;
    }

    set role(value: string) {
        this._role = value;
    }

    public static fromJson(data:JSON):User{
        let user:User = new User();
        user.name = data["name"];
        user.role = data["role"];
        return user;
    }
}