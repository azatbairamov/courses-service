export default class RegistrationForm{
    private _login:string = "";
    private _password:string = "";
    private _email:string = "";
    private _name:string = "";
    private _documentNumber:number = 0;

    valid(){
        let errors:Array<string> = [];

        if(this.login.length > 32 || this.login.length < 6){
            errors.push("Логин должен быть от 6 до 32 символов")
        }
        if(this.password.length > 32 || this.password.length < 6){
            errors.push("Пароль должен быть от 6 до 32 символов")
        }
        if(this.email.length > 32 || this.email.length < 6){
            errors.push("Почта должна быть от 6 до 32 символов")
        }
        if(this.name.length > 32 || this.name.length < 2){
            errors.push("Имя должно быть от 2 до 32 символов")
        }
        if(this.documentNumber < 100000 || this.documentNumber > 999999){
            errors.push("Номер студенческого должен быть из 6 чисел")
        }
        return errors;
    }

    get login(): string {
        return this._login;
    }

    set login(value: string) {
        this._login = value;
    }

    get password(): string {
        return this._password;
    }

    set password(value: string) {
        this._password = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get documentNumber(): number {
        return this._documentNumber;
    }

    set documentNumber(value: number) {
        this._documentNumber = value;
    }

    toJson(){
        return {
            "login": this.login,
            "password": this.password,
            "email": this.email,
            "name": this.name,
            "documentNumber": this.documentNumber
        }
    }

}