export default class LoginForm{
    private _login:string = "";
    private _password:string = "";

    valid(){
        let errors:Array<string> = [];

        if(this.login.length > 32 || this.login.length < 6){
            errors.push("Логин должен быть от 6 до 32 символов")
        }
        if(this.password.length > 32 || this.password.length < 6){
            errors.push("Пароль должен быть от 6 до 32 символов")
        }

        return errors;
    }
    get login(): string {
        return this._login;
    }

    set login(value: string) {
        this._login = value;
    }

    get password(): string {
        return this._password;
    }

    set password(value: string) {
        this._password = value;
    }

    toJson(){
        return {
            "login": this.login,
            "password": this.password,
        }
    }

}