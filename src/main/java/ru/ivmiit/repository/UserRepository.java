package ru.ivmiit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import ru.ivmiit.model.User;

import java.util.Optional;
import java.util.UUID;

@Service
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findOneByLogin(String login);

    Optional<User> findByUuid(UUID uuid);

}
