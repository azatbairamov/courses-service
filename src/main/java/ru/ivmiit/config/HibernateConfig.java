package ru.ivmiit.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.support.TransactionTemplate;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
@EnableJpaRepositories("ru.ivmiit.repository")
@EnableTransactionManagement
public class HibernateConfig {

    @Lazy
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf) {
        return new JpaTransactionManager(emf);
    }

    @Lazy
    @Bean(name = "transactionTemplate")
    public TransactionTemplate transactionTemplate(PlatformTransactionManager transactionManager) {
        TransactionTemplate transactionTemplate = new TransactionTemplate(transactionManager);
        transactionTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
        return transactionTemplate;
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, @Autowired @Qualifier("jpaProperties") Properties properties) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.ivmiit.model");
        factoryBean.setDataSource(dataSource);
        factoryBean.setBootstrapExecutor(new SimpleAsyncTaskExecutor("jpa-bootstrap"));
        factoryBean.setPersistenceUnitName("GibddmonPU");
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public HikariDataSource resourcesProperties() throws ClassNotFoundException {
        return dataSource("/database.properties");
    }

    private HikariDataSource dataSource(String propFilePath) throws ClassNotFoundException {
        return new HikariDataSource(new HikariConfig(propFilePath));
    }

    @Bean
    @Qualifier("jpaProperties")
    public Properties jpaProperties() throws ClassNotFoundException {
        return dataSourceByName("jpa.properties");
    }

    //
    private Properties dataSourceByName(String propFileName) throws ClassNotFoundException {
        Properties prop = new Properties();

        try {
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new ClassNotFoundException("Property file '" + propFileName + "' not found in the classpath");
            }
        } catch (IOException e) {
            throw new ClassNotFoundException("Property file '" + propFileName + "' IO exception: " + e.getMessage());

        }
        return prop;
    }

}