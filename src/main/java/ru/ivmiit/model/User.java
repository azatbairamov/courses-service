package ru.ivmiit.model;

import lombok.*;
import ru.ivmiit.security.role.Role;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "service_user")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    //Номер зачётки
    private Integer documentNumber;

    private String login;

    private String passwordHash;

    @Enumerated(EnumType.STRING)
    private Role role;

    private String mail;

    //Для подтверждения
    private UUID uuid;

    private boolean isTeacher;
    //Активирован ли аккаунт
    //Если учитель, то пока false нет доступа
    //Если ученик, то не может записаться на курсы
    private boolean isActivated;

    private boolean isConfirmed;

    private boolean isDeleted;

}