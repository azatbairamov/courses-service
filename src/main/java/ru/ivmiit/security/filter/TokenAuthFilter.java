package ru.ivmiit.security.filter;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.ivmiit.security.token.TokenAuthentication;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.function.Supplier;

@Component
public class TokenAuthFilter implements Filter {

    public static String tokenName = "token";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        Cookie tokenCookie;
        if(request.getCookies() != null) {
            tokenCookie = Arrays.stream(request.getCookies())
                    .filter(e -> e.getName().equals(tokenName))
                    .findFirst()
                    .orElseGet(() -> new Cookie(tokenName, null));

        }else {
            tokenCookie = new Cookie(tokenName, null);
        }
        TokenAuthentication tokenAuthentication = new TokenAuthentication(tokenCookie.getValue());
        if (tokenCookie.getValue() == null || tokenCookie.getValue().length() == 0) {
            tokenAuthentication.setAuthenticated(false);
        } else {
            SecurityContextHolder.getContext().setAuthentication(tokenAuthentication);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
}