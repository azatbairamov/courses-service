package ru.ivmiit.security.role;

public enum Role {
    STUDENT, TEACHER, ADMIN
}
