package ru.ivmiit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ivmiit.model.User;
import ru.ivmiit.service.AuthenticationService;
import ru.ivmiit.transfer.UserDto;

@RestController
public class ProfilePage {

    @Autowired
    private AuthenticationService authService;

    @Transactional
    @GetMapping("/profile")
    @PreAuthorize("hasRole('STUDENT') or hasRole('TEACHER') or  hasRole('ADMIN')")
    public UserDto getProfile(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = authService.getUserByAuthentication(authentication);
        return UserDto.from(user);
    }
}
