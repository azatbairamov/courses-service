package ru.ivmiit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.ivmiit.forms.LoginForm;
import ru.ivmiit.security.filter.TokenAuthFilter;
import ru.ivmiit.service.LoginService;
import ru.ivmiit.transfer.TokenDto;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
public class LoginController {

    @Autowired
    private LoginService loginService;

    @PostMapping("/login")
    public ResponseEntity<TokenDto> login(@RequestBody LoginForm loginForm, HttpServletResponse response) {
        try {
            TokenDto tokenDto = loginService.login(loginForm);
            response.addCookie(new Cookie(TokenAuthFilter.tokenName, tokenDto.getValue()));
            return ResponseEntity.ok(tokenDto);
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @PostMapping("/revoke-token")
    public ResponseEntity<String> logout(HttpServletResponse response) {

        loginService.logout(response);
        return ResponseEntity.ok().body("");
    }
}