package ru.ivmiit.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.ivmiit.forms.UserRegistrationForm;
import ru.ivmiit.model.User;
import ru.ivmiit.repository.UserRepository;
import ru.ivmiit.service.RegistrationService;
import ru.ivmiit.validators.UserRegistrationFormValidator;

import java.util.Optional;
import java.util.UUID;

@RestController
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @PostMapping("/register")
    public ResponseEntity<Object> addUser(@RequestBody UserRegistrationForm userForm) {
        registrationService.register(userForm);
        return ResponseEntity.ok().build();
    }
}