package ru.ivmiit.transfer;

import lombok.*;
import ru.ivmiit.model.User;
import ru.ivmiit.security.role.Role;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserDto {

    private Long id;

    private String name;

    private Integer documentNumber;

    private String login;

    private Role role;

    private String mail;

    private boolean isTeacher;

    public static UserDto from(User user){
        return UserDto.builder()
                .id(user.getId())
                .name(user.getName())
                .documentNumber(user.getDocumentNumber())
                .login(user.getLogin())
                .role(user.getRole())
                .mail(user.getMail())
                .isTeacher(user.isTeacher())
                .build();
    }
}

