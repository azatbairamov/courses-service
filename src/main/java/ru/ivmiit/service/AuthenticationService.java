package ru.ivmiit.service;

import org.springframework.security.core.Authentication;
import ru.ivmiit.model.User;

public interface AuthenticationService {
    User getUserByAuthentication(Authentication authentication);
}
