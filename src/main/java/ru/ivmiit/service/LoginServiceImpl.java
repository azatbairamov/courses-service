package ru.ivmiit.service;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.ivmiit.forms.LoginForm;
import ru.ivmiit.model.Token;
import ru.ivmiit.model.User;
import ru.ivmiit.repository.TokenRepository;
import ru.ivmiit.repository.UserRepository;
import ru.ivmiit.security.filter.TokenAuthFilter;
import ru.ivmiit.service.LoginService;
import ru.ivmiit.transfer.TokenDto;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

import static ru.ivmiit.transfer.TokenDto.from;

@Component
public class LoginServiceImpl implements LoginService {

    @Autowired
    private TokenRepository tokenRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Override
    public TokenDto login(LoginForm loginForm) {
        Optional<User> userCandidate = userRepository.findOneByLogin(loginForm.getLogin());

        if (userCandidate.isPresent()) {
            User user = userCandidate.get();

            if (passwordEncoder.matches(loginForm.getPassword(), user.getPasswordHash())) {
                Token token = Token.builder()
                        .user(user)
                        .value(RandomStringUtils.random(10, true, true))
                        .build();

                tokenRepository.save(token);
                return from(token);
            }
        } throw new IllegalArgumentException("User not found");
    }

    @Override
    public void logout(HttpServletResponse response) {

        response.addCookie(new Cookie(TokenAuthFilter.tokenName, null));

    }
}