package ru.ivmiit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ivmiit.forms.UserRegistrationForm;
import ru.ivmiit.model.User;
import ru.ivmiit.repository.UserRepository;
import ru.ivmiit.security.role.Role;

import java.util.UUID;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    private String host = "";

    @Autowired
    private UserRepository userRepository;


    @Override
    public void register(UserRegistrationForm userForm) {
        UUID uuid = UUID.randomUUID();
        User newUser = User.builder()
                .name(userForm.getName())
                .documentNumber(userForm.getDocumentNumber())
                .login(userForm.getLogin())
                .passwordHash(passwordEncoder.encode(userForm.getPassword()))
                .role(Role.STUDENT)
                .isTeacher(false)
                .mail(userForm.getEmail())
                .uuid(uuid)
                .isTeacher(false)
                .isActivated(false)
                .isConfirmed(false)
                .isDeleted(false)
                .build();
        userRepository.save(newUser);
    }

}