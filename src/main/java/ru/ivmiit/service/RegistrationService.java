package ru.ivmiit.service;

import ru.ivmiit.forms.UserRegistrationForm;
import ru.ivmiit.model.User;

public interface RegistrationService {
    void register(UserRegistrationForm userForm);

}
